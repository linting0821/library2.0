package com.tuyano.dao;

//検索に必要な本のデータの追加
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import com.tuyano.entity.SearchData;


	public class SearchDataDaoImpl implements SearchDataDao<SearchData> {
		
		private static final long serialVersionUID = 1L;
	    private EntityManager entityManager;
	
	    public SearchDataDaoImpl(){
		super();
	}
	    public SearchDataDaoImpl(EntityManager manager){
		this();
		entityManager = manager;
	}
	
//	@Override
//	public List<SearchData> getAll() {
//		Query query = entityManager.createQuery("from SearchData");
//		@SuppressWarnings("unchecked")
//		List<SearchData> list = query.getResultList();
//		entityManager.close();
//		return list;
//	}
//	
//	//ID（行番号）での検索
//	public SearchData findById(long id) {
//		return (SearchData)entityManager.createQuery("from SearchData where id = " 
//			+ id).getSingleResult();
//	}
	@Override
	@SuppressWarnings("unchecked")
	public List<SearchData> find(String fstr){
		List<SearchData> list = null;
//		String qstr = "from SearchData where typeId = :typeId or typeName like :typeName or keyword like :keyword";
		String qstr = "from SearchData where typeName like :typeName or keyword like :keyword or bookName like :bookName";
//		Long fid = 0L;
//		try {
//			fid = Long.parseLong(fstr);
//		} catch (NumberFormatException e) {
//			e.printStackTrace();
//		}
		Query query = entityManager.createQuery(qstr)
//				.setParameter("typeId", fstr)
				.setParameter("typeName", "%" + fstr + "%")
				.setParameter("keyword","%" + fstr + "%")
		        .setParameter("bookName","%" + fstr + "%");
		list = query.getResultList();
		return list;
	}
//	@Override
//	public List<SearchData> findByName(String name) {
//		// TODO 自動生成されたメソッド・スタブ
//		return null;
//	}
//	@Override
//	public List<SearchData> find(String fstr) {
//		// TODO 自動生成されたメソッド・スタブ
//		return ;
//	}

}

