package com.tuyano.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.tuyano.service.LoginUserService;

@Controller
public class LoginAuthController {

    @Autowired
    LoginUserService userService;

    @RequestMapping("/")
    public String inde() {
        return "redirect:/top";
    }
    
    @GetMapping("/login")
    public String index() {
        return "index";
    }

//    @PostMapping("/login")
//    public String loginPost() {
//        return "redirect:/login-error";
//    }

    @GetMapping("/login-error")
    public String loginError(Model model) {
        model.addAttribute("loginError", true);
        return "index";
    }

    @GetMapping("/top")
    public String top() {
        return "top";
    }

    @PostMapping("/top")
    public String postTop() {
        return "top";
    }

}