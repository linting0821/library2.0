package com.tuyano.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class HeloController {
	
	@RequestMapping("/top")
	public ModelAndView top(ModelAndView mav) {
		mav.setViewName("top");
		return mav;
	}
	
	@RequestMapping("/detail")
	public ModelAndView detail(ModelAndView mav) {
		mav.setViewName("detail");
		return mav;
	}
	
	@RequestMapping("/office")
	public ModelAndView office(ModelAndView mav) {
		mav.setViewName("office");
		return mav;
	}
	
	@RequestMapping("/java")
	public ModelAndView java(ModelAndView mav) {
		mav.setViewName("java");
		return mav;
	}
	
	@RequestMapping("/marketing")
	public ModelAndView marketing(ModelAndView mav) {
		mav.setViewName("marketing");
		return mav;
	}
	
	@RequestMapping("/inspire")
	public ModelAndView inspire(ModelAndView mav) {
		mav.setViewName("inspire");
		return mav;
	}

	@RequestMapping("/searchResult")
	public ModelAndView searchResult(ModelAndView mav) {
		mav.setViewName("searchResult");
		return mav;
	}
}
