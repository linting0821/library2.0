package com.tuyano.repository;

//本のデータリポジトリ
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tuyano.entity.SearchData;

@Repository
public interface SearchDataRepository  extends JpaRepository<SearchData, Long>{ 

}
