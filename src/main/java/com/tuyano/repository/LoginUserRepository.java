package com.tuyano.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.tuyano.entity.LoginUserData;

@Repository
public interface LoginUserRepository extends CrudRepository<LoginUserData, Long> {

    public LoginUserData findByUsername(String username);

    public LoginUserData findByMailAddress(String mailAddress);
}