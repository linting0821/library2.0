package com.tuyano.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.tuyano.service.LoginUserService;

@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private LoginUserService userService;

    @Override
    public void configure(WebSecurity web) throws Exception {
      // @formatter:off
      web
        .debug(false)
        .ignoring()
        .antMatchers("/images/**", "/js/**", "/css/**")
      ;
      // @formatter:on
    }
    
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .formLogin()
                .loginPage("/login")
                .defaultSuccessUrl("/")
                .failureUrl("/login-error")
                .permitAll();
        http
            .authorizeRequests()
            .antMatchers("/", "/signup", "/login", "/login-error",
                    "/zipcode", "/ziplist",
                    "/rest",
                    "/shopSearch", "/shopResult").permitAll()
            .antMatchers("/zipcode/** ").hasRole("USER")
                .antMatchers("/**").hasRole("USER")
                .anyRequest().authenticated();

        //403error�ｿｽ�ｿｽ�ｿｽo�ｿｽ�ｿｽ鼾�ｿｽi�ｿｽb�ｿｽ�ｿｽBCSRF�ｿｽﾎ搾ｿｽﾌゑｿｽ�ｿｽﾟ、�ｿｽ{�ｿｽﾔでは擾ｿｽ�ｿｽ�ｿｽ�ｿｽﾄはゑｿｽ�ｿｽ�ｿｽ�ｿｽﾜゑｿｽ�ｿｽ�ｿｽBCSRF�ｿｽﾎ搾ｿｽ�ｿｽg�ｿｽﾝ搾ｿｽ�ｿｽﾝまゑｿｽ�ｿｽ蛯､�ｿｽj
        http
            .csrf().disable();
        http
        	.headers().frameOptions().sameOrigin();
    }


//    //TODO �ｿｽC�ｿｽ�ｿｽ�ｿｽf�ｿｽ�ｿｽ�ｿｽg�ｿｽｼゑｿｽ�ｿｽﾄゑｿｽ�ｿｽ�ｿｽ�ｿｽ�ｿｽ�ｿｽ�ｿｽ
//  @Override
//  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//    
//      List<LoginUserData> userList = userService.getAll();
//
//      //data�ｿｽ�ｿｽ�ｿｽ�ｿｽﾌ場合�ｿｽﾌみ、�ｿｽT�ｿｽ[�ｿｽo�ｿｽN�ｿｽ�ｿｽ�ｿｽ�ｿｽ�ｿｽﾉデ�ｿｽ[�ｿｽ^�ｿｽo�ｿｽ^�ｿｽ�ｿｽ�ｿｽ�ｿｽ�ｿｽ�ｿｽ�ｿｽs�ｿｽ�ｿｽ
//      if (userList.isEmpty()) {
//          auth
//              .userDetailsService(userService)
//              .passwordEncoder(passwordEncoder());
//
//          userService.registerAdmin(20190001, "�ｿｽR�ｿｽc�ｿｽ�ｿｽ�ｿｽY", "12345678", "20190001@e-library.com");
//          userService.registerAdmin(20190002, "�ｿｽﾉ難ｿｽ�ｿｽ�ｿｽY", "12345679", "20190002@e-library.com");
//          userService.registerAdmin(20190003, "�ｿｽR�ｿｽ�ｿｽ�ｿｽﾔ子", "12345680", "20190003@e-library.com");
//          userService.registerAdmin(20190004, "�ｿｽ�ｿｽ�ｿｽ�ｿｽ�ｿｽ�ｿｽ�ｿｽG", "12345681", "20190004@e-library.com");
//          userService.registerAdmin(20190005, "�ｿｽﾔ田�ｿｽ�ｿｽ�ｿｽ�ｿｽ", "12345682", "20190005@e-library.com");
//          userService.registerAdmin(20190006, "�ｿｽ�ｿｽ�ｿｽc�ｿｽﾘ厄ｿｽ", "12345683", "20190006@e-library.com");
//          userService.registerAdmin(20190007, "�ｿｽX�ｿｽ�ｿｽM�ｿｽM", "12345684", "20190007@e-library.com");
//          userService.registerAdmin(20190008, "�ｿｽ�ｿｽ�ｿｽV�ｿｽm�ｿｽ�ｿｽ", "12345685", "20190008@e-library.com");
//          userService.registerAdmin(20190009, "�ｿｽﾘ托ｿｽ�ｿｽ�ｿｽ", "12345686", "20190009@e-library.com");
//          userService.registerAdmin(20190010, "�ｿｽ�ｿｽ�ｿｽ�ｿｽ�ｿｽC�ｿｽi", "12345687", "20190010@e-library.com");
//      }
//  }

    
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}